public class ListaEncadeada {
    
        private NoLista primeiro;
        private NoLista ultimo;
        private int total = 0;
    
        public ListaEncadeada() {}
    
        public void addComeco(String nomeUsuario, String email, int sala) {// adiciona um novo nó no começo da lista
            NoLista novo = new NoLista(this.primeiro, nomeUsuario, email, sala);
            this.primeiro = novo;
    
            if (total == 0) {// lista esteja vazia
                this.ultimo = this.primeiro;
            }
            this.total++;
    
        }
    
        public void adicionar(String nomeUsuario, String email, int sala) {// adiciona o nó no começo ou no final da lista
            if (total == 0) {
                addComeco(nomeUsuario, email, sala);
            } else {
                NoLista novo = new NoLista(this.primeiro, nomeUsuario, email, sala);
                this.ultimo.setProximo(novo);// coloco no fim da lista
                this.ultimo = novo;// coloco no fim da lista nessa classe
                this.total++;
            }
        }
    
        public String print() {// mostra o que contem na lista
    
            if (total == 0) {// verifica se ha dados na lista
                return "Ninguem foi agraciado";
            }
            
            String resultado = "";
            NoLista atual = primeiro;

            for (int i = 0; i < total; i++) {
                resultado = resultado + atual.toString();
                atual = atual.getProximo();

            }
               
            return resultado;
        }
    
        private boolean posicaoOcupada(int pos) {// verifica se tem algum item na posicao
            if ((pos >= 0) && (pos < this.total)) {
                return true;
            }
            return false;
    
        }    
 
        public void removeComeco() {// remove um item do começo
    
            if (!this.posicaoOcupada(0)) {
                throw new IllegalArgumentException("Essa posicao nao existe");
            }
    
            this.primeiro = this.primeiro.getProximo();
            this.total--;
    
            if (this.total == 0) {
                this.ultimo = null;
            }
        }
    
}
