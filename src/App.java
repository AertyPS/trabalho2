
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;


public class App {

    public static void main(String[] args) throws IOException {
        
        int opcao = -1; // Opção para trabalhar o menu principal
        int aleAux = 0, aleAux2 = 0, tamFila = 0,cont2 = 0;
        int numSala = 0,i1 = 0,i2 = 0,i3 = 0,i4 = 0,i5 = 0,contLog2 = 0,contLog3 = 0;
        String log2 ="",log2Aux ="",log3 = "";
        
        // Criando um objeto para trabalhar dados pelo teclado.
        Scanner entrada = new Scanner(System.in);
        
        // Criando uma instância da FilaContigua.
        FilaContigua filaPessoas = new FilaContigua();

        // Criando uma instância da PilhaEncadeada.
        PilhaEncadeada pihaIngressos = new PilhaEncadeada();

        // Criando uma instância da ListaEncadeada.
        ListaEncadeada listaAgraciados = new ListaEncadeada();

        int[] eventoSala = {1,2,3,4,5};//criar e preencher o evento de cada sala para embaralhamento

        log2 += "Quantidade de eventos [5]\n";
        log2 += "Quantidade de salas [5]  \n\n";
                
        for (int j = 0; j < eventoSala.length; j++) {//embaralhamento eventos que ocorrerao nas salas

            aleAux = 1 + (int) (Math.random() * 5);//os eventos vao de 1 ate 5
            aleAux2 = eventoSala[j];
            eventoSala[j] = aleAux;
            for (int i = 0; i < eventoSala.length; i++) {//fazendo a troca dentro do vetor
                if((eventoSala[i] == aleAux)&&(i!=j)){
                    eventoSala[i] = aleAux2;
                }
            }
        
        }

        for (int i = 0; i < eventoSala.length; i++) {//item  do log2
            log2 += "Sala ["+i+"] evento ["+eventoSala[i]+"]\n";
        }
        
        tamFila = 1+(int) (Math.random() * 103);//tamanho da fila aleatorio     
        filaPessoas.queue(tamFila);//setando tamanho da fila
        filaPessoas = Leitura("pessoas.txt", tamFila);//preenchimento da fila com arquivo pessoas.txt

        //a quantidade de ingresso esta entre de 5 ate o tamanho da fila 
         int qtdpihaIngressos = 5 + (int) (Math.random() * (tamFila - 5));
        
        if(qtdpihaIngressos > 75){//no maximo 15 pessoas por sala
            qtdpihaIngressos = 75;//caso ultrapasse 15 pessoas por sala
        }

        contLog2 = qtdpihaIngressos;
        //preechendo a pilha de pihaIngressos com salas aleatorias pegando o log2 sem ultrapassar 15 pessoas por sala
        while (qtdpihaIngressos > 0) {
            numSala =  (int) (Math.random() * 5);

            if(numSala == 0){
                i1++;
                if(i1 < 16){
                    pihaIngressos.push(numSala, eventoSala[numSala]);
                    log2Aux ="ELEMENTO["+(contLog2 - qtdpihaIngressos)+"] [ Sala: " + numSala + " | " + "Evento: " + eventoSala[numSala] + " ]\n"+log2Aux;
                    qtdpihaIngressos --;
                }

            }else if(numSala == 1){
                i2++;
                if(i2 < 16){
                    pihaIngressos.push(numSala, eventoSala[numSala]);
                    log2Aux ="ELEMENTO["+(contLog2 - qtdpihaIngressos)+"] [ Sala: " + numSala + " | " + "Evento: " + eventoSala[numSala] + " ]\n"+log2Aux;
                    qtdpihaIngressos --;
                }

            }else if(numSala == 2){
                i3++;
                if(i3 < 16){
                    pihaIngressos.push(numSala, eventoSala[numSala]);
                    log2Aux ="ELEMENTO["+(contLog2 - qtdpihaIngressos)+"] [ Sala: " + numSala + " | " + "Evento: " + eventoSala[numSala] + " ]\n"+log2Aux;
                    qtdpihaIngressos --;
                }
            }else if(numSala == 3){
                i4++;
                if(i4 < 16){
                    pihaIngressos.push(numSala, eventoSala[numSala]);
                    log2Aux ="ELEMENTO["+(contLog2 - qtdpihaIngressos)+"] [ Sala: " + numSala + " | " + "Evento: " + eventoSala[numSala] + " ]\n"+log2Aux;
                    qtdpihaIngressos --;
                }
            }else if(numSala == 4){
                i5++;
                if(i5 < 16){
                    pihaIngressos.push(numSala, eventoSala[numSala]);
                    log2Aux ="ELEMENTO["+(contLog2 - qtdpihaIngressos)+"] [ Sala: " + numSala + " | " + "Evento: " + eventoSala[numSala] + " ]\n"+log2Aux;
                    qtdpihaIngressos --;
                }
            } 
        }
            //concatenção dos textos exibidos no log2 na ordem correta
            log2 +="\nTOTAL DE INGRESSOS : "+contLog2+"\n";
            log2 +="\nIgressos por sala\n";
            log2 += "Sala [0] : "+i1+" ingressos\n";
            log2 += "Sala [1] : "+i2+" ingressos\n";
            log2 += "Sala [2] : "+i3+" ingressos\n";
            log2 += "Sala [3] : "+i4+" ingressos\n";
            log2 += "Sala [4] : "+i5+" ingressos\n";
            log2 += "\n";
            log2 += "Distribuição da Pilha de ingressos\n";
            log2 += log2Aux;
            

        cont2 = tamFila;
        log3 +="LISTAGEM DE AGRACIADOS\n\n";

       //preenchimento da lista encadeada das pessoas que receberam ingressos
        while(cont2 > 0){

            if(filaPessoas.isEmpty() || pihaIngressos.isEmpty()){//se acabou o ingresso ou acabou a fila
                cont2 = 0;

            }else{

                listaAgraciados.adicionar(filaPessoas.frontNome(), filaPessoas.frontEmail(), pihaIngressos.topSala());
                log3 +="ELEMENTO["+contLog3+"] [ Nome: " + filaPessoas.frontNome() + " | " + "Email: " + filaPessoas.frontEmail() + " | " + "Sala: " + pihaIngressos.topSala() + " ]\n";
                filaPessoas.dequeue(); 
                pihaIngressos.pop();
                cont2 --;
                contLog3++;
            } 
                     
        }
       
        escrita("log2.txt", log2);//escreve log2 no arquivo
        escrita("log3.txt", log3);//escreve log3 no arquivo

        // Enquanto a opção for diferente de zero.
        while (opcao != 0){
            
            System.out.println("=================================");
            System.out.println(" 1 - LISTAR FILA                 ");
            System.out.println(" 2 - LISTAR PILHA DE INGRESSOS   ");
            System.out.println(" 3 - LISTAR AGRACIADOS           ");
            System.out.println("=================================");
            System.out.println(" 0 - Sair da aplicação           ");
            System.out.println("=================================");
            
            
            System.out.print("Opção = ");
           
           
            opcao = entrada.nextInt();
            
            System.out.print("\n");
            
            switch (opcao){

                case 0:	System.out.println("---------------------");
                        System.out.println(" SAINDO DA APLICAÇÃO ");
                        System.out.println("---------------------");

                        entrada.close();

                        break;
                
                case 1: System.out.println("PESSOAS QUE ESTÃO NA FILA SEM INGRESSO\n\n"+ filaPessoas.print());
                       
                        break;

                case 2:	System.out.println("LISTAGEM DE INGRESSOS DISPONIVEIS\n\n"+ pihaIngressos.print());

                        break;
                
                case 3: System.out.print("LISTAGEM DE PESSOAS AGRACIADAS\n\n"+listaAgraciados.print());
                       
                        break;
                
                default: System.out.println("Entrada inválida!");
                
            }
            
            System.out.print("\n");
    
        }
        
    }

    //função que escreve no arquivo log1,log2,log3
    public static void escrita(String t,String d) throws IOException{

        FileWriter fw = null;
        PrintWriter saida = null;
        String texto = t;//nome do arquivo.txt
        String dados = d;//dados a serem inseridos no log

        File arq = new File(texto);//criando arquivo de saida se nao existir

        if(arq.exists()){
            System.out.println("\nO arquivo "+texto +" ja existia");
        }else{
            System.out.println("\nO arquivo "+ texto +" nao existia");
            try {
                Path file = Paths.get(texto);
                Files.createFile(file);
            } catch (IOException e) {
                System.out.println("Erro ao criar arquivo");
            }
        }
      
        String conteudo ="";//variavel que armazenara o texto do arquivo    
        conteudo +="===================================================================================================\n";
        conteudo +="                                       Inicio da Execução                                          \n";
        conteudo +="===================================================================================================\n";
        conteudo += dados;
        conteudo +="===================================================================================================\n";
        conteudo +="                                       Fim da Execução                                             \n";
        conteudo +="===================================================================================================\n";
        try {
           fw = new FileWriter(arq);
           saida = new PrintWriter(fw);
           saida.println(conteudo);

        } catch (IOException e) {
            System.out.println("Erro ao escrever no arquivo");
        }finally{
            saida.close();
            fw.close();
        }

    }

    //leitura arquivo pessoas
    public static FilaContigua Leitura(String text, int q) throws IOException {

        FileReader fr = null;
        BufferedReader br = null;
        String texto = text;
        String log1 ="Tamanho da fila: "+q+"\n";//gera o log da fila
        int qtd = q, contador = 0;//quantidade de linhas desejado/contador para log1
        FilaContigua fp = new FilaContigua();
        fp.queue(q);
        String nome = "", email = "";

        try {

            fr = new FileReader(texto);
            br = new BufferedReader(fr);
            String linha = br.readLine();
            linha = br.readLine();
            boolean ativar = false;

            while (qtd > 0) {// dessossador de arquivo

                StringReader reader = new StringReader(linha);
                int k = 0;

                while ((k = reader.read()) != -1) { // desossando a linha

                    if ((char) k == ':') {
                        ativar = true;
                    } 

                    if(ativar){
                        if((char) k != ':'){
                            email += (char) k;
                        }     
                    }else{
                        nome += (char) k;
                    }

                }
                ativar = false;
                fp.enqueue(email, nome);//insere os dados na fila contigua
                log1 +="ELEMENTO["+contador+"] [ Nome: " + nome + " | " + "Email: " + email + " ]\n";
                contador++;
                nome = "";
                email = "";

                linha = br.readLine();//p fazer a leitura da proxima linha
                qtd--;
            }

        } catch (FileNotFoundException e) {
            System.out.println("Arquivo nao encontrado");

        } catch (IOException e) {
            System.out.println("Erro na leitura");

        } finally {

            fr.close();
            br.close();
            log1 +="Pessoas inseridas na fila com sucesso!!!\n";
            escrita("log1.txt", log1);
        }
        return fp;
    }


    
}

