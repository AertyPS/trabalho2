public class NoFila {
    
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
    -----------------------------------*/
    
    private String email;
    private String nomeUsuario;
    
    /*-----------------------------------
            CONSTRUTOR DA CLASSE
    -----------------------------------*/
    
    public NoFila(String email, String nomeUsuario) {
        this.email = email;
        this.nomeUsuario   = nomeUsuario;
    }
    
    /*-----------------------------------
                MÉTODOS DA CLASSE
    -----------------------------------*/
    
    // Método para resgatar o email do usuário
    public String getEmail() {
        return this.email;
    }

    // Método para registrar o email do usuário
    public void setEmail(String email) {
        this.email = email;
    }

    // Método para resgatar o nome do usuário
    public String getNomeUsuario() {
        return this.nomeUsuario;
    }

    // Método para registrar o nome do usuário
    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    // Método que retorna os dados do usuário.
    public String toString(){

        return " [ Nome: " + this.nomeUsuario + " | " + "Email: " + this.email + " ]\n";

    }
    
}

