public class NoLista {
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
    -----------------------------------*/

    private String nomeUsuario;
    private String email;
    private int sala;
    private NoLista proximo;
    
    /*-----------------------------------
            CONSTRUTOR DA CLASSE
    -----------------------------------*/
    
    public NoLista(NoLista no,String nomeUsuario, String email, int sala) {
        this.email = email;
        this.nomeUsuario   = nomeUsuario;
        this.sala = sala;
        this.proximo = no;
    }
    
    /*-----------------------------------
                MÉTODOS DA CLASSE
    -----------------------------------*/
    
    // Método para resgatar o email do usuário
    public String getEmail() {
        return email;
    }

    // Método para registrar o email do usuário
    public void setEmail(String email) {
        this.email = email;
    }

    // Método para resgatar o nome do usuário
    public String getNomeUsuario() {
        return nomeUsuario;
    }

    // Método para registrar o nome do usuário
    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    //metodo para retornar a sala
    public int getSala() {
        return sala;
    }

    //metodo para inserir a sala
    public void setSala(int sala) {
        this.sala = sala;
    }

    public NoLista getProximo() {
        return this.proximo;
    }

    public void setProximo(NoLista proximo) {
        this.proximo = proximo;
    }

    // Método que retorna os dados do usuário.
    public String toString(){

        return " [ Nome: " + this.nomeUsuario + " | " + "Email: " + this.email + " | " + "Sala: " + this.sala + " ]\n";

    }


}
