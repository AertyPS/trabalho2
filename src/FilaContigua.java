
public class FilaContigua {
    
    private NoFila Fila[];
    private int finalDaFila;

    public FilaContigua(){ }

    // Criar a fila
    public void queue(int tf){
        Fila = new NoFila[tf];
        finalDaFila = -1;
    }
    // verifica se a fila esta vazia
    public boolean isEmpty(){
        return finalDaFila == -1;
    }
    // verifica se a fila esta cheia
    public boolean isFull(){
        return finalDaFila == (Fila.length-1);
    }

    // Insere um conteúdo (enfileirar - Inserir)
    public boolean enqueue(String email, String nome){

        if ( !isFull() ){

            NoFila novoNo = new NoFila(email,nome);

            finalDaFila++;
            Fila[finalDaFila] = novoNo;
            
            return true;

        }

        return false; // Quando a fila estiver cheia.

    }

    // Remover um elemento da fila
    public String dequeue(){

        String resultado = front();

        if ( !resultado.equals("") ){

            for (int i = 0; i < finalDaFila; i++){
                Fila[i] = Fila[i+1];
            }
            
            finalDaFila--;

        }

        return resultado;

    }

    public String frontNome(){
        if ( !isEmpty() )
            return Fila[0].getNomeUsuario();
        return "";
    }

    public String frontEmail(){
        if ( !isEmpty() )
            return Fila[0].getEmail();
        return "";
    }

    public String front(){
        if ( !isEmpty() )
            return Fila[0].toString();
        return "";
    }
    
    public int size(){
        return (Fila.length-1);
    }

    public void clear(){
        finalDaFila = -1;
    }

    public String print(){

        String resultado = "";

        if ( !isEmpty() ){

            for (int i = 0; i <= finalDaFila; i++){
                resultado = resultado + Fila[i].toString();
            }  
            
        }else{
            resultado = "Não tem pessoas na fila";
        }

        return resultado;

    }

}
