public class NoPilha {
    /*-----------------------------------
              ATRIBUTOS DA CLASSE
    -----------------------------------*/
    
    private int sala;
    private int evento;//tipo de evento na sala filme, palestra...
    
    private NoPilha proximo;
    
    /*-----------------------------------
            CONSTRUTOR DA CLASSE
    -----------------------------------*/
    
    public NoPilha(int sala, int evento) {
        this.sala = sala;
        this.evento = evento;
    }
    
    /*-----------------------------------
                MÉTODOS DA CLASSE
    -----------------------------------*/
    
    // Método para resgatar o sala do usuário
    public int getSala() {
        return sala;
    }

    // Método para resgatar o evento da sala
    public int getEvento() {
        return evento;
    }
  
    public NoPilha getProximo() {
        return this.proximo;
    }
    
    public void setProximo(NoPilha proximo) {
        this.proximo = proximo;
    }

        // Método que retorna os dados.
     public String toString(){

            return " [ Sala: " + this.sala + " | " + "Evento: " + this.evento + " ]\n";
    
        }

}
