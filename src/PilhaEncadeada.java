public class PilhaEncadeada {

        private NoPilha topo;
        private int tamanhoDaPilha;
    
        public PilhaEncadeada(){
            topo = null;
            tamanhoDaPilha = 0;
        }
    
        public boolean isEmpty(){
            return topo == null;
        }
    
        public void push(int sala,int evento){//insere
    
            NoPilha novoNo = new NoPilha(sala, evento);
    
            if ( isEmpty() ){
                topo = novoNo;
                novoNo.setProximo(null);
            }else{
                novoNo.setProximo(topo);
                topo = novoNo;
            }
    
            tamanhoDaPilha++;
    
        }
    
        public String pop(){//deleta do topo
    
            String resultado = "";
    
            if ( !isEmpty() ){
    
                resultado = top();
    
                NoPilha auxiliar = topo;
    
                topo = topo.getProximo();
                auxiliar.setProximo(null);
    
                tamanhoDaPilha--;
                
            }
    
            return resultado; 
    
        }
    
        public String top(){
            if ( !isEmpty() )
                return topo.toString();
            return "";
        }
        
        public int topSala(){
            if ( !isEmpty() )
                return topo.getSala();
            return -1;
        }

        public String topEvento(){
            if ( !isEmpty() )
                return ""+topo.getEvento();
            return "";
        }
       
        public int size(){
    
            return tamanhoDaPilha;
    
        }
        //retorna os dados da pilha 
        public String print(){
    
            String resultado = "";
    
            if ( !isEmpty() ){
    
                NoPilha auxiliar = topo;
                
                while ( auxiliar != null ){
                    resultado = resultado + auxiliar.toString();
                    auxiliar = auxiliar.getProximo();
                }
                
            }else{
                resultado = "Ingressos Esgotados";
            }
    
            return resultado;
    
        }

}
